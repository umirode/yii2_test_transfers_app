<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transfers`.
 */
class m180603_125052_create_transfers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transfers', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'amount' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('transfers');
    }
}
