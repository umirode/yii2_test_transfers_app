<?php

use yii\db\Migration;

/**
 * Handles adding balance to table `users`.
 */
class m180603_124500_add_balance_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'balance', $this->integer()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'balance');
    }
}
