<?php
/**
 * Created by IntelliJ IDEA.
 * User: maksim
 * Date: 03.06.18
 * Time: 13:47
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <h1>Регистрация</h1>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'passwordConfirm')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>