<?php
/**
 * Created by IntelliJ IDEA.
 * User: maksim
 * Date: 03.06.18
 * Time: 15:20
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<h1>Вход</h1>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'rememberMe')->checkbox([
    'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
]) ?>

<div class="form-group">
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
