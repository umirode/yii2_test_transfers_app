<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */
/* @var $form ActiveForm */
?>

<h1>Новый перевод</h1>

<?php $form = ActiveForm::begin(); ?>
<h4>Коммисия</h4>
<ul>
    <?php foreach ($commissions as $commission): ?>
        <li><?= $commission['desc'] ?>
            <?= $commission['percentage'] ?>%
            <?= $commission['minSum'] ? ',  но не менее ' . $commission['minSum'] . ' руб.' : '' ?></li>
    <?php endforeach; ?>
</ul>

<?= $form->field($model, 'amount') ?>
<?= $form->field($model, 'to_email') ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
