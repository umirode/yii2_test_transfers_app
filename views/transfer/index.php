<?php
/* @var $this yii\web\View */

$outputTransfersProvider = new \yii\data\ArrayDataProvider([
    'allModels' => $outputTransfers,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['from', 'to', 'amount', 'created_at'],
    ],
]);

$inputTransfersProvider = new \yii\data\ArrayDataProvider([
    'allModels' => $inputTransfers,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['from', 'to', 'amount', 'created_at'],
    ],
])
?>

<h1>Переводы</h1>

<h2>Входящие</h2>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $inputTransfersProvider,
    'columns' => [
        'from',
        'to',
        'amount',
        'created_at'
    ],
]); ?>


<h2>Исходящие</h2>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $outputTransfersProvider,
    'columns' => [
        'from',
        'to',
        'amount',
        'created_at'
    ],
]); ?>
