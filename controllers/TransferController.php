<?php

namespace app\controllers;

use app\models\CreateTransferForm;
use app\models\Transfer;
use app\models\User;
use Yii;

/**
 * Class TransferController
 * @package app\controllers
 */
class TransferController extends \yii\web\Controller
{
    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['auth/login']);
        }

        $currentUser = $this->getCurrentUser();

        return $this->render('index', [
            'inputTransfers' => $this->getTransfersWithEmails($currentUser->inputTransfers, 'in'),
            'outputTransfers' => $this->getTransfersWithEmails($currentUser->outputTransfers, 'out')
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['auth/login']);
        }

        $model = new CreateTransferForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $currentUser = $this->getCurrentUser();
            $transferUser = User::findOne([
                'email' => $model->to_email
            ]);

            $currentUser->balance -= $model->amount;
            $currentUser->save();

            $commissionSum = Transfer::getCommissionSum($model->amount);
            $model->amount -= $commissionSum;

            $transfer = new Transfer();
            $transfer->link('fromUser', $currentUser);
            $transfer->link('toUser', $transferUser);
            $transfer->amount = $model->amount;
            $transfer->save();

            $transferUser->balance += $model->amount;
            $transferUser->save();

            return $this->redirect(['transfer/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'commissions' => Transfer::$commissions
        ]);
    }

    /**
     * @param array $transfers
     * @param string $type
     * @return array
     */
    private function getTransfersWithEmails($transfers, $type)
    {
        $currentUser = $this->getCurrentUser();

        $transfersWithEmails = [];

        foreach ($transfers as $transfer) {
            $transferWithEmail = [
                'from' => '',
                'to' => '',
                'amount' => $transfer->amount,
                'created_at' => $transfer->created_at
            ];

            if ($type === 'in') {
                $transferWithEmail['from'] = User::find()
                    ->select('email')
                    ->where(['id' => $transfer->from_id])
                    ->one()
                    ->email;
                $transferWithEmail['to'] = $currentUser->email;
            } elseif ($type === 'out') {
                $transferWithEmail['from'] = $currentUser->email;
                $transferWithEmail['to'] = User::find()
                    ->select('email')
                    ->where(['id' => $transfer->to_id])
                    ->one()
                    ->email;
            }

            array_push($transfersWithEmails, $transferWithEmail);
        }

        return $transfersWithEmails;
    }

    /**
     * @return User|null
     */
    private function getCurrentUser()
    {
        return User::findOne(\Yii::$app->user->id);
    }
}
