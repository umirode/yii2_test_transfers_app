<?php
/**
 * Created by IntelliJ IDEA.
 * User: maksim
 * Date: 03.06.18
 * Time: 13:24
 */

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use app\models\SignupForm;

/**
 * Class AuthController
 * @package app\controllers
 */
class AuthController extends \yii\web\Controller
{
    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['transfer/index']);
        }

        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->email = $model->email;
            $user->name = $model->name;
            $user->password = Yii::$app->security->generatePasswordHash($model->password);
            $user->balance = 100000;
            $user->save();

            return $this->redirect(['auth/login']);
        } else {
            return $this->render('signup.php', ['model' => $model]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['transfer/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['transfer/index']);
        }

        $model->password = '';
        return $this->render('login.php', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }

        return $this->redirect(['auth/login']);
    }
}