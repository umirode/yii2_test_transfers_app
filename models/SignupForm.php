<?php
/**
 * Created by IntelliJ IDEA.
 * User: maksim
 * Date: 03.06.18
 * Time: 13:14
 */

namespace app\models;

use yii\base\Model;

/**
 * Class SignupForm
 * @package app\models
 */
class SignupForm extends Model
{
    /**
     * @var string $email
     */
    public $email;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $password
     */
    public $password;

    /**
     * @var string $passwordConfirm
     */
    public $passwordConfirm;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'name', 'password', 'passwordConfirm'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            [['passwordConfirm'], 'compare', 'compareAttribute' => 'password'],
        ];
    }
}