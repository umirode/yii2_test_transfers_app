<?php
/**
 * Created by IntelliJ IDEA.
 * User: maksim
 * Date: 03.06.18
 * Time: 13:14
 */

namespace app\models;

use yii\base\Model;

/**
 * Class CreateTransferForm
 * @package app\models
 */
class CreateTransferForm extends Model
{
    /**
     * @var string $to_email
     */
    public $to_email;

    /**
     * @var integer $amount
     */
    public $amount;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['to_email', 'amount'], 'required'],
            ['to_email', 'email'],
            ['to_email', 'validateEmail'],
            ['amount', 'integer'],
            ['amount', 'validateAmount'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currentUser = $this->getCurrentUser();
            $user = User::findOne([
                'email' => $this->to_email
            ]);

            if (!$user) {
                $this->addError($attribute, 'Incorrect email.');
            } elseif ($currentUser->email == $user->email) {
                $this->addError($attribute, 'It is your email.');
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currentUser = $this->getCurrentUser();

            $commissionSum = Transfer::getCommissionSum($this->amount);
            $commissionSum++;

            if ($commissionSum > $this->amount) {
                $this->addError($attribute, 'Min ' . $commissionSum);
            } elseif ($currentUser->balance < $this->amount) {
                $this->addError($attribute, 'Not enough money.');
            }
        }
    }

    /**
     * @return User|null
     */
    private function getCurrentUser()
    {
        return User::findOne(\Yii::$app->user->id);
    }
}