<?php

namespace app\models;


/**
 * This is the model class for table "transfers".
 *
 * @property int $id
 * @property string $created_at
 * @property int $from_id
 * @property int $to_id
 * @property int $amount
 */
class Transfer extends \yii\db\ActiveRecord
{
    /**
     * @var array $commissions
     */
    public static $commissions = [
        [
            'percentage' => 10,
            'minSum' => 0,
            'desc' => 'За перевод средств.'
        ],
        [
            'percentage' => 1,
            'minSum' => 44,
            'desc' => 'За секретность.'
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transfers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['from_id', 'to_id', 'amount'], 'integer'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne('app\models\User', ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne('app\models\User', ['id' => 'to_id']);
    }

    /**
     * @param $amount
     * @return integer
     */
    public static function getCommissionSum($amount)
    {
        $commissions = self::$commissions;
        $commissionMinSum = 0;

        foreach ($commissions as $commission) {
            $commissionSum = $amount / 100 * $commission['percentage'];
            if ($commissionSum < $commission['minSum']) {
                $commissionMinSum += $commission['minSum'];
            } else {
                $commissionMinSum += $commissionSum;
            }
        }

        return ceil($commissionMinSum);
    }
}
